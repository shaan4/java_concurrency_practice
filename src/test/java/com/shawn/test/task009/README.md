Task 010
任务描述：
读完 Chapter 12(page 292): Testing Concurrent Programs
代码实验 针对 Task 009 的 SynchronizedGrocery 和你改进之后的版本
基于JSR166TestCase (https://github.com/google/guava/blob/master/guava-tests/test/com/google/common/util/concurrent/JSR166TestCase.java) 正确性测试
基于JMH https://github.com/openjdk/jmh 进行性能测试
Deadline：5月10日中午12点
最先完成任务且无明显缺陷的前3名同学将各获得一枚免死金牌 🏅️
指出前三名明显缺陷的同学可以抢得🏅️

正确性测试
- [ImprovedGroceryTest.java](ImprovedGroceryTest.java)
- [SynchronizedGroceryTest.java](SynchronizedGroceryTest.java)

性能测试
- [Task009Main.java](https://gitlab.com/shaan4/java_concurrency_practice/-/blob/main/src/main/java/com/shawn/test/task009/Task0009Main.java)

![截图](./JMH%20Visual%20Chart.png)
