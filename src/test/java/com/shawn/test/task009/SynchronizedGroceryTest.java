package com.shawn.test.task009;

import junit.framework.Test;
import junit.framework.TestSuite;

public class SynchronizedGroceryTest extends JSR166TestCase {

    public void testIsFullWithNullWhenConstructed() {
        Grocery grocery = new SynchronizedGrocery(10);

        assertEquals(10, grocery.getFruits().size());
        assertEquals(10, grocery.getVegetables().size());
        for (String fruit : grocery.getFruits()) {
            assertNull(fruit);
        }
        for (String fruit : grocery.getVegetables()) {
            assertNull(fruit);
        }
    }

    public void testIsNotNullWhenAddAFruit() {
        Grocery grocery = new SynchronizedGrocery(10);
        new Thread(() -> {

            grocery.addFruit(0, "apple");
            threadAssertEquals("apple", grocery.getFruits().get(0));
        }).start();

    }

    public void testIsNotNullWhenAddVegetable() {
        Grocery grocery = new SynchronizedGrocery(10);

        new Thread(() -> {
            grocery.addVegetable(0, "onion");
            assertEquals("onion", grocery.getVegetables().get(0));
        }).start();
    }

    public void testIsNotNullWhenMultipleThreadAdds(){
        Grocery grocery = new SynchronizedGrocery(10);

        new Thread(() -> {
            grocery.addVegetable(0, "onion");
            assertEquals("onion", grocery.getVegetables().get(0));
        }).start();
        new Thread(() -> {
            grocery.addFruit(0, "apple");
            threadAssertEquals("apple", grocery.getFruits().get(0));
        }).start();
    }
}
