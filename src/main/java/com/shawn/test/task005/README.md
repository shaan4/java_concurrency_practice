## Task 005
任务描述：
 - 读完 Chapter 6 (page 142)
 - 改进 Listing 5.11 提供的 TestHarness 工具，新类名 ImprovedTestHarness，要求：
 - 使用 CyclicBarrier 而不是 CountDownLatch;
 - 不得使用 unbounded thread;
 - 分别使用 ImprovedTestHarness 和 Listing 5.11 提供的TestHarness 对 Task 004进行简单(非严格)的度量，比较结果；

Deadline：3月15日中午12点

最先完成任务且无明显缺陷的前3名同学将各获得一枚免死金牌 🏅️

指出前三名明显缺陷的同学可以抢得🏅️

### Task005Main.java - 运行结果
```
系统启动成功，创建线程池，最大线程:10
10个线程，每个线程同时添加100000个元素，最终所需时间如下结果如下:
warmup...
warmup completed

ImprovedTestHarness
improvedMap: 		1009564677
currentHashMap: 	761792659
synchronizedMap: 	835918519

TestHarness
improvedMap: 		1133032165
currentHashMap: 	529323503
synchronizedMap: 	1001210236
系统关闭成功
```
