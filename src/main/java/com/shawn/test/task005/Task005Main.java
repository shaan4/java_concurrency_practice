package com.shawn.test.task005;

import com.shawn.test.task003.ImprovedMap;
import com.shawn.test.task004.Task004Main;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.IntStream;

public class Task005Main {
    public static void main(String[] args) throws Exception {
        int nThreads = 10;
        Task005Configuration task005Configuration = new Task005Configuration();
        task005Configuration.init(nThreads);

        try {
            int maxItemsPerThread = 100000;
            System.out.println(nThreads + "个线程，每个线程同时添加" + maxItemsPerThread + "个元素，最终所需时间如下结果如下:");
            System.out.println("warmup...");
            IntStream.range(0, 10).forEach((index) -> {
                try {
                    getMapPerformanceByImprovedTestHarness(nThreads, maxItemsPerThread, new ImprovedMap<>());
                    getMapPerformanceByImprovedTestHarness(nThreads, maxItemsPerThread, new ConcurrentHashMap<>());
                    getMapPerformanceByImprovedTestHarness(nThreads, maxItemsPerThread, Collections.synchronizedMap(new HashMap<>()));
                    getMapPerformanceByTestHarness(nThreads, maxItemsPerThread, new ImprovedMap<>());
                    getMapPerformanceByTestHarness(nThreads, maxItemsPerThread, new ConcurrentHashMap<>());
                    getMapPerformanceByTestHarness(nThreads, maxItemsPerThread, Collections.synchronizedMap(new HashMap<>()));
                } catch (Exception e) {
                    //ignore
                }
            });
            System.out.println("warmup completed");
            System.out.println("\nImprovedTestHarness");
            long improvedMapCost = getMapPerformanceByImprovedTestHarness(nThreads, maxItemsPerThread, new ImprovedMap<>());
            System.out.println("improvedMap: \t\t" + improvedMapCost);
            long currentHashMapCost = getMapPerformanceByImprovedTestHarness(nThreads, maxItemsPerThread, new ConcurrentHashMap<>());
            System.out.println("currentHashMap: \t" + currentHashMapCost);
            long synchronizedMapCost = getMapPerformanceByImprovedTestHarness(nThreads, maxItemsPerThread, Collections.synchronizedMap(new HashMap<>()));
            System.out.println("synchronizedMap: \t" + synchronizedMapCost);

            System.out.println("\nTestHarness");
            improvedMapCost = getMapPerformanceByTestHarness(nThreads, maxItemsPerThread, new ImprovedMap<>());
            System.out.println("improvedMap: \t\t" + improvedMapCost);
            currentHashMapCost = getMapPerformanceByTestHarness(nThreads, maxItemsPerThread, new ConcurrentHashMap<>());
            System.out.println("currentHashMap: \t" + currentHashMapCost);
            synchronizedMapCost = getMapPerformanceByTestHarness(nThreads, maxItemsPerThread, Collections.synchronizedMap(new HashMap<>()));
            System.out.println("synchronizedMap: \t" + synchronizedMapCost);
        } finally {
            task005Configuration.shutdown();
        }
    }

    private static long getMapPerformanceByImprovedTestHarness(int nThreads, int maxItemsPerThread, Map<String, String> improvedMap) throws Exception {
        return ImprovedTestHarness.timeTasks(nThreads, () -> {
            IntStream.range(0, maxItemsPerThread).forEach(itemsNo -> {
                improvedMap.put(String.format("%09d", itemsNo) + Thread.currentThread().getName(), "1");
            });
        });
    }

    private static long getMapPerformanceByTestHarness(int nThreads, int maxItemsPerThread, Map<String, String> improvedMap) throws InterruptedException, ExecutionException {
        return Task004Main.timeTasks(nThreads, () -> {
            IntStream.range(0, maxItemsPerThread).forEach(itemsNo -> {
                improvedMap.put(String.format("%09d", itemsNo) + Thread.currentThread().getName(), "1");
            });
        });
    }
}
