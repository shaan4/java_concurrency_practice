package com.shawn.test.task005;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class Task005Configuration {

    private static ThreadPoolExecutor threadPoolExecutor;

    public static ThreadPoolExecutor getThreadPoolExecutor() {
        return threadPoolExecutor;
    }

    public void init(int maxThreadPoolCount) {
        System.out.println("系统启动成功，创建线程池，最大线程:" + maxThreadPoolCount);
        threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(maxThreadPoolCount);
    }

    public void shutdown() {
        threadPoolExecutor.shutdown();
        threadPoolExecutor = null;
        System.out.println("系统关闭成功");
    }
}
