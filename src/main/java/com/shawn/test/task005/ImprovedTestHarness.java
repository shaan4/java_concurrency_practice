package com.shawn.test.task005;

import java.util.concurrent.*;

public class ImprovedTestHarness {


    public static long timeTasks(int nThreads, final Runnable task) throws Exception {
        ThreadPoolExecutor threadPoolExecutor = Task005Configuration.getThreadPoolExecutor();
        if (threadPoolExecutor == null) {
            throw new Exception("The thread pool is not initialized.");
        }
        if (nThreads > threadPoolExecutor.getMaximumPoolSize()) {
            throw new Exception("The number of threads exceeds the size of the maximum pool, which will cause the method to not end correctly");
        }

        Callable<Long> getNanoTime = () -> System.nanoTime();

        FutureTask<Long> startFutureTask = new FutureTask<>(getNanoTime);
        CyclicBarrier startBarrier = new CyclicBarrier(nThreads, startFutureTask);

        FutureTask<Long> endFutureTask = new FutureTask<>(getNanoTime);
        CyclicBarrier endBarrier = new CyclicBarrier(nThreads, endFutureTask);

        for (int i = 0; i < nThreads; i++) {
            threadPoolExecutor.submit(() -> {
                try {
                    startBarrier.await();
                    task.run();
                    endBarrier.await();
                } catch (InterruptedException | BrokenBarrierException ignored) {
                    // ignored
                }
            });
        }

        long start = startFutureTask.get();
        long end = endFutureTask.get();
        return end - start;
    }
}
