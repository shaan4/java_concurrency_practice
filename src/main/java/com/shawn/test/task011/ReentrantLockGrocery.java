package com.shawn.test.task011;

import com.shawn.test.task009.Grocery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

public class ReentrantLockGrocery implements Grocery {

    private final List<String> fruits = new ArrayList<>();
    private final List<String> vegetables = new ArrayList<>();

    private Lock lock;

    ReentrantLockGrocery(int size, boolean usefairLock) {
        IntStream.range(0, size).forEach(index -> {
            fruits.add(null);
            vegetables.add(null);
        });
        lock = new ReentrantLock(usefairLock);
    }

    @Override
    public void addFruit(int index, String fruit) {
        lock.lock();
        try {
            fruits.add(index, fruit);
        }finally {
            lock.unlock();
        }
    }

    public void addVegetable(int index, String vegetable) {
        lock.lock();
        try {
            vegetables.add(index, vegetable);
        }finally {
            lock.unlock();
        }
    }

    @Override
    public List<String> getFruits() {
        return Collections.unmodifiableList(fruits);
    }

    @Override
    public List<String> getVegetables() {
        return Collections.unmodifiableList(vegetables);
    }
}
