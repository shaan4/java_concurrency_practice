Task 011

任务描述：

读完 Chapter 13(page 208): Explicit lock
代码实验
使用 ReentrantLock 改进 Task 009 的 SynchronizedGrocery
分别使用非公平锁(nonfair lock ) 和 公平锁 (fair lock) 实现并基于JMH https://github.com/openjdk/jmh 进行性能对比

Deadline：5月22日中午12点

最先完成任务且无明显缺陷的前3名同学将各获得一枚免死金牌 🏅️
指出前三名明显缺陷的同学可以抢得🏅️


### Task011Main.java - 运行结果

```
Benchmark                   (numThreads)   Mode  Cnt   Score   Error  Units
Task011Main.fairGrocery                2  thrpt   25  55.491 ± 5.778  ops/s
Task011Main.fairGrocery                4  thrpt   25  26.889 ± 2.923  ops/s
Task011Main.fairGrocery                6  thrpt   25  18.010 ± 1.791  ops/s
Task011Main.fairGrocery                8  thrpt   25  13.341 ± 1.248  ops/s
Task011Main.fairGrocery               10  thrpt   25  10.762 ± 1.090  ops/s
Task011Main.fairGrocery               12  thrpt   25   9.015 ± 0.798  ops/s
Task011Main.nonfairGrocery             2  thrpt   25  68.842 ± 7.824  ops/s
Task011Main.nonfairGrocery             4  thrpt   25  34.161 ± 4.029  ops/s
Task011Main.nonfairGrocery             6  thrpt   25  23.003 ± 2.984  ops/s
Task011Main.nonfairGrocery             8  thrpt   25  16.945 ± 2.136  ops/s
Task011Main.nonfairGrocery            10  thrpt   25  13.885 ± 1.622  ops/s
Task011Main.nonfairGrocery            12  thrpt   25  11.322 ± 1.475  ops/s
```
![截图](./JMH%20Visual%20Chart.png)

