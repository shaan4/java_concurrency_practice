package com.shawn.test.task011;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@State(Scope.Benchmark)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 5, time = 2)
public class Task011Main {

    @Param(value = {"2", "4", "6", "8", "10", "12"})
    public int numThreads;
    private ReentrantLockGrocery fairGrocery;
    private ReentrantLockGrocery nonfairGrocery;

    @Setup(Level.Trial)
    public void setup() {
        fairGrocery = new ReentrantLockGrocery(10, true);
        nonfairGrocery = new ReentrantLockGrocery(10, false);
    }

    @Benchmark
    public void fairGrocery() throws InterruptedException {
        Thread[] threads = new Thread[numThreads];
        for (int i = 0; i < numThreads; i++) {
            threads[i] = new Thread(() -> {
                for (int j = 0; j < 100; j++) {
                    fairGrocery.addFruit(1, "apple");
                }
            });
            threads[i].start();
        }
        for (Thread thread : threads) {
            thread.join();
        }
    }

    @Benchmark
    public void nonfairGrocery() throws InterruptedException {
        Thread[] threads = new Thread[numThreads];
        for (int i = 0; i < numThreads; i++) {
            threads[i] = new Thread(() -> {

                for (int j = 0; j < 100; j++) {
                    nonfairGrocery.addFruit(1, "apple");
                }
            });
            threads[i].start();
        }
        for (Thread thread : threads) {
            thread.join();
        }
    }

    public static void main(String[] args) throws IOException, RunnerException {
//        org.openjdk.jmh.Main.main(args);
        Options opts = new OptionsBuilder()
                .include(Task011Main.class.getSimpleName())
                .resultFormat(ResultFormatType.JSON)
                .build();
        new Runner(opts).run();
    }
}
