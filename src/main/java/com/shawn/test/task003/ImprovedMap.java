package com.shawn.test.task003;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ImprovedMap<K, V> implements Map<K, V> {
    private final Map<K, V> myMap = new HashMap<>();

    public synchronized V get(Object key) {
        return myMap.get(key);
    }

    public synchronized V put(K key, V value) {
        return myMap.put(key, value);
    }

    @Override
    public synchronized V remove(Object o) {
        return myMap.remove(o);
    }

    @Override
    public synchronized void putAll(Map<? extends K, ? extends V> map) {
        myMap.putAll(map);
    }

    @Override
    public synchronized void clear() {
        myMap.clear();
    }

    @Override
    public synchronized Set<K> keySet() {
        return myMap.keySet();
    }

    @Override
    public synchronized Collection<V> values() {
        return myMap.values();
    }

    @Override
    public synchronized Set<Entry<K, V>> entrySet() {
        return myMap.entrySet();
    }

    public synchronized int size() {
        return myMap.size();
    }

    @Override
    public synchronized boolean isEmpty() {
        return myMap.isEmpty();
    }

    @Override
    public synchronized boolean containsKey(Object o) {
        return myMap.containsKey(o);
    }

    @Override
    public synchronized boolean containsValue(Object o) {
        return myMap.containsValue(o);
    }
}
