package com.shawn.test.task003;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.stream.IntStream;

public class TestHashMapCircleReference {
    private String str;
    public TestHashMapCircleReference(String str){
        this.str = str;
    }

    @Override
    public int hashCode() {
        return 1;
    }

    public static void main(String[] args) throws InterruptedException {

        Map<TestHashMapCircleReference,String> map = new HashMap<>();
        List<Thread> threads = new ArrayList<>();
        int maxThreads = 20;
        int maxAddItems = 3000;
        CountDownLatch countDownLatch = new CountDownLatch(maxThreads);
        IntStream.range(0,maxThreads).forEach((i)->{
            int finalI1 = i;
            threads.add(new Thread(() -> {
                IntStream.range(0, maxAddItems).forEach((curr -> map.put(new TestHashMapCircleReference(String.format("%02d%04d", finalI1,curr)), Thread.currentThread().getName())));
                countDownLatch.countDown();
            }));
        });
        for (Thread thread: threads) {
            thread.start();
        }
        countDownLatch.await();
        System.out.println(map.size());
        System.out.println(maxThreads*maxAddItems);
    }
}
