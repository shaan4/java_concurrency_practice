package com.shawn.test.task003;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

public class TestMap {
    private String name;

    public TestMap(String n) {
        this.name = n;
    }

    @Override
    public int hashCode() {
        return 1;
    }

    public static void main(String[] args) throws InterruptedException {
        Map<TestMap, String> map = new HashMap<>();
        List<Thread> threads = new ArrayList<>();
        int threadCount = 100;
        int retryCount = 3000;
        CountDownLatch countDownLatch = new CountDownLatch(threadCount);

        for (int threadNo = 0; threadNo < threadCount; threadNo++) {
            int finalThreadNo = threadNo;
            threads.add(new Thread(() -> {
                for (int innerIndex = 0; innerIndex < retryCount; innerIndex++) {
                    map.put(new TestMap(String.format("%04d%04d", finalThreadNo, innerIndex)), "1");
                }
                countDownLatch.countDown();
            }));
        }

        threads.forEach(thread -> thread.start());
        countDownLatch.await();
        System.out.println(map.size());
    }
}
