package com.shawn.test.task003;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestMapIsSafeOrNot {

    public static void main(String[] args) throws InterruptedException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        System.out.println("------------------------------------------");
        testMapIsThreadSafe(HashMap.class);
        System.out.println("------------------------------------------");
        testMapIsThreadSafe(ImprovedMap.class);
        System.out.println("------------------------------------------");
    }

    public static void testMapIsThreadSafe(Class<?> clazz) throws InterruptedException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        String isThreadSafe = "是";
        int currentRetryTimes = 0;
        int maxRetryTimes = 1000;
        int threadCounts = 20;
        int itemsSize = 40;

        System.out.println("开始测试[" + clazz.getSimpleName() + "]，每次实例化一个新的对象，启动" + threadCounts + "个线程向其添加" + itemsSize + "个元素,期待最后元素个数应该等于线程数" + threadCounts * itemsSize + ".");

        while (currentRetryTimes < maxRetryTimes) {
            currentRetryTimes++;
            Map<String, String> map = (Map<String, String>) clazz.getConstructor().newInstance();
            CountDownLatch countDownLatch = new CountDownLatch(threadCounts);
            List<Thread> threads = new ArrayList<>();
            for (int index = 0; index < threadCounts; index++) {
                threads.add(new Thread(() -> {
                    for (int nums = 0; nums < itemsSize; nums++) {
                        map.put(Thread.currentThread().getName() + nums, "1");
                    }
                    countDownLatch.countDown();
                }));
            }
            for (Thread thread : threads) {
                thread.start();
            }
            countDownLatch.await();
            if (threadCounts * itemsSize != map.size()) {
                isThreadSafe = "不是";
                break;
            }
        }
        System.out.println("测试完成，共测试" + currentRetryTimes + "次");
        System.out.println("是否线程安全? " + isThreadSafe);
    }
}
