
## 任务描述：- 读完 Chapter 4 通过封装 `HashMap`实现一个线程安全的 `Map`
1. 无需实现所有的方法，只实现验证所需要用到的方法，比如 `put(K key, V value)` 即可。
2. 验证:`HaspMap#put`非线程安全；而 `ImprovedMap#put`线程安全。

### TestMapIsSafeOrNot.java - 运行结果

```
------------------------------------------
开始测试[HashMap]，每次实例化一个新的对象，启动20个线程向其添加40个元素,期待最后元素个数应该等于线程数800.
测试完成，共测试1次
是否线程安全? 不是
------------------------------------------
开始测试[ImprovedMap]，每次实例化一个新的对象，启动20个线程向其添加40个元素,期待最后元素个数应该等于线程数800.
测试完成，共测试1000次
是否线程安全? 是
------------------------------------------
```
