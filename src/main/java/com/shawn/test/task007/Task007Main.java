package com.shawn.test.task007;

import com.shawn.test.task006.Task006Main;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class Task007Main {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        AtomicInteger failedCount = new AtomicInteger(0);
        int nThreads = 20;
        int timeoutInSeconds = 10;

        Runnable runnable = () -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                failedCount.addAndGet(1);
            }
        };
        for (int i = 0; i < 20; i++) {
            //warmup
            new Task007Main().timeTasks(nThreads, timeoutInSeconds, runnable, true);
            new Task007Main().timeTasks(nThreads, timeoutInSeconds, runnable, false);
        }
        long preStartAllCoreThreadPoolRuntime = new Task007Main().timeTasks(nThreads, timeoutInSeconds, runnable, true);


        long notPreStartAllCoreThreadPoolRuntime = new Task007Main().timeTasks(nThreads, timeoutInSeconds, runnable,
                false);
        System.out.println("pre start all core thread total time is \t\t" + preStartAllCoreThreadPoolRuntime + " " +
                "milliseconds");
        System.out.println("not pre start all core thread total time is \t" + notPreStartAllCoreThreadPoolRuntime + " " +
                "milliseconds");
//		3月 26, 2023 8:31:36 下午 com.shawn.test.task007.TimingThreadPool terminated
//		信息: Terminated:isPreStartCoreThread true avg 1007494175ns
//		3月 26, 2023 8:31:37 下午 com.shawn.test.task007.TimingThreadPool terminated
//		信息: Terminated:isPreStartCoreThread false avg 1007522674ns
//		pre start all core thread total time is 		1004 milliseconds
//		not pre start all core thread total time is 	1006 milliseconds
    }


    /**
     * @param nThreads
     * @param timeoutInSeconds
     * @param task
     * @return cost seconds
     * @throws Exception
     */
    private long timeTasks(int nThreads, int timeoutInSeconds, Runnable task, boolean shouldStartAllCoreThread) throws InterruptedException, ExecutionException {

        ExecutorService trackingThreadPool = new TimingThreadPool(nThreads, shouldStartAllCoreThread);
        Callable<Long> getNanoTime = () -> System.currentTimeMillis();

        FutureTask<Long> startFutureTask = new FutureTask<>(getNanoTime);
        CyclicBarrier startBarrier = new CyclicBarrier(nThreads, startFutureTask);

        FutureTask<Long> endFutureTask = new FutureTask<>(getNanoTime);
        CyclicBarrier endBarrier = new CyclicBarrier(nThreads, endFutureTask);

        for (int i = 0; i < nThreads; i++) {
            trackingThreadPool.execute(() -> {
                try {
                    startBarrier.await();
                    runTaskWithTimeout(timeoutInSeconds, task);
                } catch (InterruptedException | BrokenBarrierException ignore) {
                    // ignore
                } finally {
                    try {
                        endBarrier.await();
                    } catch (InterruptedException | BrokenBarrierException ignore) {
                        // ignore
                    }
                }
            });
        }

        trackingThreadPool.shutdown();
        long start = startFutureTask.get();
        long end = endFutureTask.get();
        return end - start;// seconds
    }

    private void runTaskWithTimeout(int timeoutInSeconds, Runnable task) throws InterruptedException {
        final ExecutorService workThreadPool = Executors.newSingleThreadExecutor();
        final Future<?> submittedJob = workThreadPool.submit(task);
        workThreadPool.shutdown();

        try {
            submittedJob.get(timeoutInSeconds, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException ignore) {

        } finally {
            submittedJob.cancel(true);
        }
        workThreadPool.awaitTermination(10, TimeUnit.SECONDS);
    }
}
