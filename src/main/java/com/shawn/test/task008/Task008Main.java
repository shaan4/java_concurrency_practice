package com.shawn.test.task008;

import com.shawn.test.task007.TimingThreadPool;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Task008Main {

    public static void main(String[] args) {
        Object lock1 = new Object();
        Object lock2 = new Object();
        new Thread(()->{
            synchronized (lock1){
                wait(1);
                synchronized (lock2){
                    System.out.println("do something in thread 1");
                }
            }
        }).start();
        new Thread(()->{
            synchronized (lock2){
                wait(1);
                synchronized (lock1){
                    System.out.println("do something in thread 1");
                }
            }
        }).start();
    }

    private static void wait(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException ignore) {

        }
    }

}
