## Task 008
![任务描述](./task%20descripiton.png)


### Task008Main.java - 运行结果

- 触发 thread dump 并截图
![任务描述](./trigger%20thead%20dump%20screenshot.png)
- 使用 jstack 查看死锁信息并截图
![任务描述](./jstack%20screenshot.png)
- 使用 jconsole 查看死锁信息并截图
![任务描述](./jconsole%20screenshot.png)

