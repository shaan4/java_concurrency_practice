
## 任务描述：- 读完 Chapter 2 ~ 3 (page 58)- 写一段代码

### Q1: 验证 Listing 3.6 所描述的问题: 其他线程可以访问并修改数组的内容.

A1: UnsafeState001 - 运行结果

```
线程1修改第一个原素的值为YOUR ARE WRONG!
线程2获取第一个元素，发现值被线程1改为YOUR ARE WRONG!
```

### Q2: 验证加上final后变成 `private final String[] states` 验证问题是否依然存在.

A2: UnsafeState002 - 运行结果

```
线程1修改第一个原素的值为YOUR ARE WRONG!
线程2获取第一个元素，发现值被线程1改为YOUR ARE WRONG!
```
### Q3: 修复这个问题.
```java

public String[] getStates() {                             
    // 每次都返回新的引用地址
    return states.clone();
}
```
A3: UnsafeState003 - 运行结果

```
线程1修改第一个原素的值为YOUR ARE WRONG!
线程2获取第一个元素，发现值不变
```
