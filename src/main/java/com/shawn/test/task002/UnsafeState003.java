package com.shawn.test.task002;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class UnsafeState003 {
    private String[] states = new String[]{"AK", "AL"};

    public String[] getStates() {
        // 每次都返回新的引用地址
        return states.clone();
    }

    /**
     * 修复这个问题
     * 运行结果
     * 线程1修改第一个原素的值为YOUR ARE WRONG!
     * 线程2获取第一个元素，发现值不变
     * @param args
     */
    public static void main(String[] args) {
        UnsafeState003 unsafe = new UnsafeState003();
        // change the first item in array
        new Thread(() -> {
            String[] arrays = unsafe.getStates();
            arrays[0] = "YOUR ARE WRONG!";
            System.out.println("线程1修改第一个原素的值为"+ arrays[0]);
        }).start();
        new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String[] arrays = unsafe.getStates();

            if(!"AK".equals(arrays[0])){
                System.out.println("线程2获取第一个元素，发现值被线程1改为"+ arrays[0]);
            }else
                System.out.println("线程2获取第一个元素，发现值不变");
        }).start();
    }
}
