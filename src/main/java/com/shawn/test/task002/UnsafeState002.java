package com.shawn.test.task002;

public class UnsafeState002 {
    private final String[] states = new String[]{"AK", "AL"};

    public String[] getStates() {
        return states;
    }

    /**
     * 加上final后变成 `private final String[] states` 验证问题依然存在
     * 运行结果
     * 线程1修改第一个原素的值为YOUR ARE WRONG!
     * 线程2获取第一个元素，发现值被线程1改为YOUR ARE WRONG!
     * @param args
     */
    public static void main(String[] args) {
        UnsafeState002 unsafe = new UnsafeState002();
        // change the first item in array
        new Thread(() -> {
            String[] arrays = unsafe.getStates();
            arrays[0] = "YOUR ARE WRONG!";
            System.out.println("线程1修改第一个原素的值为"+ arrays[0]);
        }).start();
        new Thread(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String[] arrays = unsafe.getStates();

            if(!"AK".equals(arrays[0])){
                System.out.println("线程2获取第一个元素，发现值被线程1改为"+ arrays[0]);
            }else
                System.out.println("线程2获取第一个元素，发现值不变");
        }).start();
    }
}
