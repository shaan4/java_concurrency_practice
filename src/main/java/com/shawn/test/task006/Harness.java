package com.shawn.test.task006;

public interface Harness {
    long timeTasks(int nThreads, int timeoutInSeconds, final Runnable task) throws Exception;
}
