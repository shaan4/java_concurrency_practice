package com.shawn.test.task006;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Task006Main implements Harness {

    /**
     * @param nThreads
     * @param timeoutInSeconds
     * @param task
     * @return cost seconds
     * @throws Exception
     */
    public long timeTasks(int nThreads, int timeoutInSeconds, final Runnable task) throws Exception {

        ExecutorService trackingThreadPool = Executors.newFixedThreadPool(nThreads);

        Callable<Long> getNanoTime = () -> System.currentTimeMillis();

        FutureTask<Long> startFutureTask = new FutureTask<>(getNanoTime);
        CyclicBarrier startBarrier = new CyclicBarrier(nThreads, startFutureTask);

        FutureTask<Long> endFutureTask = new FutureTask<>(getNanoTime);
        CyclicBarrier endBarrier = new CyclicBarrier(nThreads, endFutureTask);

        for (int i = 0; i < nThreads; i++) {
            trackingThreadPool.execute(() -> {
                try {
                    startBarrier.await();
                    runTaskWithTimeout(timeoutInSeconds, task);
                } catch (InterruptedException | BrokenBarrierException ignore) {
                    // ignore
                } finally {
                    try {
                        endBarrier.await();
                    } catch (InterruptedException | BrokenBarrierException ignore) {
                        // ignore
                    }
                }
            });
        }

        trackingThreadPool.shutdown();
        long start = startFutureTask.get();
        long end = endFutureTask.get();
        return (end - start) / 1000;// seconds
    }

    private void runTaskWithTimeout(int timeoutInSeconds, Runnable task) {
        final ExecutorService workThreadPool = Executors.newSingleThreadExecutor();
        final Future<?> submittedJob = workThreadPool.submit(task);
        workThreadPool.shutdown();

        try {
            submittedJob.get(timeoutInSeconds, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException ignore) {
            submittedJob.cancel(true);
        }

        while (!workThreadPool.isTerminated()) {
            //wait thread pool is terminated, it may wait 53 milliseconds
        }
    }

    public static void main(String[] args) throws Exception {
        System.out.println("XXXX火箭模拟试射");
        System.out.println("3");
        System.out.println("2");
        System.out.println("1");
        System.out.println("点火");
        System.out.println("开始一级火箭分离...");
        splitRocket("助推器", 4, 7, 3);
        System.out.println("开始整流罩分离...");
        splitRocket("整流罩", 2, 7, 3);
        System.out.println("二级火箭主发动机熄火");
        System.out.println("开始箭船分离...");
        splitRocket("箭船", 1, 7, 10);
        System.out.println("呀!失败了，请下次再来吧...");
    }

    private static void splitRocket(String target, int nThreads, int timeoutInSeconds, int requiredSeconds) throws Exception {
        System.out.println("\t本次共需要分离" + nThreads + "个" + target + "(线程)，每个" + target + "(线程)分离需要" + requiredSeconds + "秒,预计在" + timeoutInSeconds + "秒内完成。");
        AtomicInteger failedCount = new AtomicInteger(0);
        long currentRuntime = new Task006Main().timeTasks(nThreads, timeoutInSeconds, () -> {
            try {
                TimeUnit.SECONDS.sleep(requiredSeconds);
            } catch (InterruptedException e) {
                failedCount.addAndGet(1);
                // InterruptedException when run timeout
//                System.out.println(Thread.currentThread().getName() + target + "(线程)分离中止!");
            }
        });
        System.out.println("\t" + target + "分离" + (failedCount.get() > 0 ? "超时被中止" : "成功!"));
        System.out.println("\t本次总运行时间是" + currentRuntime + "秒");
    }
}
