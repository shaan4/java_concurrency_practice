package com.shawn.test.task009;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 10, time = 2)
@Measurement(iterations = 10, time = 2)
@Fork(1)
@Threads(16)
public class Task0009Main {
    private static final int maxSize = 100;

    @State(Scope.Benchmark)
    public static class MyState {
        public SynchronizedGrocery synchronizedGrocery = new SynchronizedGrocery(maxSize);
        public ImprovedGrocery improvedGrocery = new ImprovedGrocery(maxSize);
    }

    @Benchmark
    @Group("SynchronizedGrocery")
    @GroupThreads(8)
    public void testSynchronizedGroceryAddFruit(MyState state) throws InterruptedException {
        state.synchronizedGrocery.addFruit(1, "fruit-");
    }

    @Benchmark
    @Group("SynchronizedGrocery")
    @GroupThreads(8)
    public void testSynchronizedGroceryAddVegetable(MyState state) throws InterruptedException {
        state.synchronizedGrocery.addVegetable(1, "vegetable-");
    }

    @Group("ImprovedGrocery")
    @GroupThreads(8)
    @Benchmark
    public void testImprovedGroceryAddFruit(MyState state) throws InterruptedException {
        state.improvedGrocery.addFruit(1, "fruit-");
    }

    @Group("ImprovedGrocery")
    @GroupThreads(8)
    @Benchmark
    public void testImprovedGroceryAddVegetable(MyState state) throws InterruptedException {
        state.improvedGrocery.addVegetable(1, "vegetable-");
    }

    public static void main(String[] args) throws RunnerException {
        Options opts = new OptionsBuilder()
                .include(Task0009Main.class.getSimpleName())
                .resultFormat(ResultFormatType.JSON)
                .build();
        new Runner(opts).run();
    }
}
