package com.shawn.test.task009;

import java.util.List;

public interface Grocery {
    void addFruit(int index, String fruit);

    void addVegetable(int index, String vegetable);

    List<String> getVegetables();
    List<String> getFruits();
}
