![任务描述](./task009%20description.png)


### Task009Main.java - 运行结果

- 16个线程 (8个线程addFruit,8个线程addVegetable)
```

Benchmark                                                              Mode  Cnt      Score      Error  Units
Task0009Main.ImprovedGrocery                                          thrpt   10  10574.886 ± 3862.042  ops/s
Task0009Main.ImprovedGrocery:testImprovedGroceryAddFruit              thrpt   10   5139.560 ± 2239.780  ops/s
Task0009Main.ImprovedGrocery:testImprovedGroceryAddVegetable          thrpt   10   5435.326 ± 1746.729  ops/s
Task0009Main.SynchronizedGrocery                                      thrpt   10   7659.058 ± 2584.807  ops/s
Task0009Main.SynchronizedGrocery:testSynchronizedGroceryAddFruit      thrpt   10   3786.381 ± 2234.039  ops/s
Task0009Main.SynchronizedGrocery:testSynchronizedGroceryAddVegetable  thrpt   10   3872.678 ± 1574.942  ops/s
```
![截图](./JMH%20Visual%20Chart.png)

