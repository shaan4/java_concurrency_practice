package com.shawn.test.task009;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

public class SynchronizedGrocery implements Grocery {
    private final List<String> fruits = new ArrayList<>();
    private final List<String> vegetables = new ArrayList<>();

    public SynchronizedGrocery(int size) {
        IntStream.range(0, size).forEach(index -> {
            fruits.add(null);
            vegetables.add(null);
        });
    }

    public synchronized void addFruit(int index, String fruit) {
        fruits.add(index, fruit);
    }

    public synchronized void addVegetable(int index, String vegetable) {
        vegetables.add(index, vegetable);
    }

    @Override
    public List<String> getFruits() {
        return Collections.unmodifiableList(fruits);
    }
    @Override
    public List<String> getVegetables() {
        return Collections.unmodifiableList(vegetables);
    }
}
