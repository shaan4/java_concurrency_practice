package com.shawn.test.task009;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.IntStream;

public class ImprovedGrocery implements Grocery {
    private final List<String> fruits = new Vector<>();
    private final List<String> vegetables = new Vector<>();

    ImprovedGrocery(int size) {
        IntStream.range(0, size).forEach(index -> {
            fruits.add(null);
            vegetables.add(null);
        });
    }

    public void addFruit(int index, String fruit) {

        fruits.add(index, fruit);
    }

    public void addVegetable(int index, String vegetable) {

        vegetables.add(index, vegetable);
    }


    @Override
    public List<String> getFruits() {
        return Collections.unmodifiableList(fruits);
    }
    @Override
    public List<String> getVegetables() {
        return Collections.unmodifiableList(vegetables);
    }
}
