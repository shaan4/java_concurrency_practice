package com.shawn.test.task001;

import java.util.*;
import java.util.concurrent.CountDownLatch;

public class VerifyNotThreadSafe {
    private int value;

    public int getNext() {
        return value++;
    }

    /**
     * 创建100个线程，每个线程调用getNext方法，判断每个线程的获取结果是否会出现相同的值，如果出现则代表该方法不是线程安全的。
     *
     * @return
     * @throws InterruptedException
     */
    public static boolean testCanGetDuplicateValue() throws InterruptedException {
        int threadNum = 100;
        VerifyNotThreadSafe testClass = new VerifyNotThreadSafe();
        /**
         * 该变量用于保存线程执行结果，及线程名字
         * 结果大致如下
         * [1,["thread1"]],
         * [2,["thread2"]],
         * [2,["thread3","thread4"]],
         */
        Map<Integer, List<String>> threadExecutedResult = Collections.synchronizedMap(new HashMap());
        // 倒计时
        CountDownLatch countdown = new CountDownLatch(threadNum);
        // create thread instance
        List<Thread> preparedThread = new ArrayList<>();
        for (int i = 0; i < threadNum; i++) {
            Thread t1 = new Thread(() -> {
                int next = testClass.getNext();
                List<String> threadNames = threadExecutedResult.get(next);
                if (null == threadExecutedResult.get(next)) {
                    threadNames = Collections.synchronizedList(new ArrayList());
                }
                threadNames.add(Thread.currentThread().getName());
                threadExecutedResult.put(next, threadNames);
                countdown.countDown();
            });
            preparedThread.add(t1);
        }
        // start thread
        preparedThread.forEach(thread -> thread.start());
        // wait all thread execute compelte
        countdown.await();

        boolean isDuplicate = false;
        // check the
        for (Integer key : threadExecutedResult.keySet()) {
            final List<String> threadNames = threadExecutedResult.get(key);
            if (threadNames.size() > 1) {
                System.out.println("线程" + threadNames + "的计算结果都是" + key);
                isDuplicate = true;
            }
        }
        return isDuplicate;
    }

    /**
     * 循环调用testCanGetDuplicateValue方法测试是否发现多个线程返回相同的值，如果证明不是线程安全，退出程序
     *
     * 运行结果如下：
     * ...
     * 进行第372次测试...
     * 进行第373次测试...
     * 进行第374次测试...
     * 进行第375次测试...
     * 进行第376次测试...
     * 线程[Thread-37580, Thread-37579]的计算结果都是79
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        int i = 0;
        while (i < 10000) {
            i++;
            System.out.println("进行第" + i + "次测试...");
            if (testCanGetDuplicateValue()) {
                System.out.println("测试完成，当多个线程获使用时会取得同样的值");
                break;
            }
        }
    }
}
