package com.shawn.test.task001;


import java.util.concurrent.*;

public class VerifyThreadSafe {
    private int Value;

    public synchronized int getNext() {
        return Value++;
    }

    /**
     * 创建二十个线程，每个线程分别获取值1000次getNext方法，如果所有线程执行完的值应该是线程数于执行次数的乘积,他就是线程安全的
     *
     * 运行结果如下：
     * 这个方法是线程安全的
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        int threadNum = 20;
        int retryTimesPerThread = 1000;
        CountDownLatch countdown = new CountDownLatch(threadNum);
        ExecutorService executor = Executors.newFixedThreadPool(100);
        VerifyThreadSafe testClass = new VerifyThreadSafe();
//        VerifyNotThreadSafe testClass = new VerifyNotThreadSafe();
        for (int i = 0; i < threadNum; i++) {
            executor.submit(() -> {
                for (int retry = 0; retry < retryTimesPerThread; retry++) {
                    testClass.getNext();
                }
                countdown.countDown();
            });
        }
        executor.shutdown();
        countdown.await();
        int currentValue = testClass.getNext();
        if (currentValue == threadNum * retryTimesPerThread) {
            System.out.println("这个方法是线程安全的");
        } else {
            System.out.println("这个方法不是线程安全的");
        }
    }
}
