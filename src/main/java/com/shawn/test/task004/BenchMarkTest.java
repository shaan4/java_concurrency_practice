package com.shawn.test.task004;

import com.shawn.test.task003.ImprovedMap;
import org.openjdk.jmh.annotations.*;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 10)
@Measurement(iterations = 10, time = 10)
@Fork(1)
@Threads(10)
public class BenchMarkTest {
    public static Map<String, String> concurrentHashMap;
    public static Map<String, String> synchronizedMap;


    @State(Scope.Benchmark)
    public static class MyState {
        public Map<String, String> concurrentHashMap = new ConcurrentHashMap<>();
        public Map<String, String> synchronizedMap = Collections.synchronizedMap(new HashMap<>());
        public Map<String, String> improvedMap = new ImprovedMap<>();
    }

    //https://www.baeldung.com/java-microbenchmark-harness
    @Benchmark
    public void testConcurrentHashMap(MyState state) throws InterruptedException {
        state.concurrentHashMap.put(Thread.currentThread().getName() + new Random(10000).nextInt(), "1");
    }

    @Benchmark
    public void testSynchronizedMap(MyState state) throws InterruptedException {
        state.synchronizedMap.put(Thread.currentThread().getName() + new Random(10000).nextInt(), "1");
    }

    @Benchmark
    public void testImprovedMap(MyState state) throws InterruptedException {
        state.improvedMap.put(Thread.currentThread().getName() + new Random(10000).nextInt(), "1");
    }


    public static void main(String[] args) throws IOException {
        org.openjdk.jmh.Main.main(args);
    }
}
