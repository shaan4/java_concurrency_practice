## Task 004任务描述：读完 Chapter 5 (page 118)测试以下3个Map 在并发场景的性能
- Collections.synchronizedMap
- ConcurrentHashMap
- Task 003 中实现的线程安全的ImprovedMap

测试方式二选一：
- 使用 Listing 5.11 提供的TestHarness 工具，进行简单(非严格)的度量；
- 采用JMH (可以参考https://confluence.kingland-apps.com/display/~biyan@ksd.kingland.cc/2022/03/25/Code+Performance+and+Microbenchmarking)，进行相对严格的度量；

Deadline：3月6日中午12点
最先完成任务且无明显缺陷的前3名同学将各获得一枚免死金牌 🏅️
指出前三名明显缺陷的同学可以抢得🏅
️使用相对严格模式证实自己的实现比ConcurrentHashMap性能更好的，可以获得🏅️一枚

### Task004Main.java - 运行结果
```
-------------------------------------------
10个线程，每个线程同时添加100000个元素，最终所需时间如下结果如下:
ImprovedMap: 1079
ConcurrentHashMap: 1142
SynchronizedMap: 1703
-------------------------------------------
冠军是ImprovedMap
-------------------------------------------
-------------------------------------------
10个线程，每个线程同时添加200000个元素，最终所需时间如下结果如下:
ConcurrentHashMap: 1867
SynchronizedMap: 2962
ImprovedMap: 3845
-------------------------------------------
冠军是ConcurrentHashMap
-------------------------------------------
-------------------------------------------
10个线程，每个线程同时添加500000个元素，最终所需时间如下结果如下:
SynchronizedMap: 5866
ConcurrentHashMap: 9510
ImprovedMap: 9906
-------------------------------------------
冠军是SynchronizedMap
-------------------------------------------
```
### BenchMarkTest.java - 运行结果
```
Benchmark                             Mode  Cnt  Score    Error   Units
BenchMarkTest.testConcurrentHashMap  thrpt   10  0.016 ±  0.001  ops/ns
BenchMarkTest.testImprovedMap        thrpt   10  0.004 ±  0.001  ops/ns
BenchMarkTest.testSynchronizedMap    thrpt   10  0.004 ±  0.001  ops/ns
```
