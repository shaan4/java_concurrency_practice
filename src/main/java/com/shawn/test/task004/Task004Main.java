package com.shawn.test.task004;

import com.shawn.test.task003.ImprovedMap;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.stream.IntStream;

public class Task004Main {

    public static void main(String[] args) throws InterruptedException {
        testMapPerformance(10, 100000);
        testMapPerformance(10, 200000);
        testMapPerformance(10, 500000);
//        testMapPerformance(20, 100000 );
//        testMapPerformance(50, 100000 );
    }
    public static void testMapPerformance(int nThreads, int maxItemsPerThread) throws InterruptedException {
        Map<String, String> synchronizedMap = Collections.synchronizedMap(new HashMap<>());
        Map<String, String> currentHashMap = new ConcurrentHashMap<>();
        Map<String, String> improvedMap = new ImprovedMap<>();

        TreeMap<Long, String> report = new TreeMap<>();
//        report.put(costTimeWhenPutManyItems(synchronizedMap, nThreads, maxItemsPerThread), "SynchronizedMap");
//        report.put(costTimeWhenPutManyItems(currentHashMap, nThreads, maxItemsPerThread), "ConcurrentHashMap");
//        report.put(costTimeWhenPutManyItems(improvedMap, nThreads, maxItemsPerThread), "ImprovedMap");

        report.put(timeTasks(nThreads, () -> IntStream.range(0, maxItemsPerThread).forEach(itemsNo -> {
            improvedMap.put(String.format("%09d", itemsNo) + Thread.currentThread().getName(), "1");
        })), "ImprovedMap");
        report.put(timeTasks(nThreads, () -> IntStream.range(0, maxItemsPerThread).forEach(itemsNo -> {
            synchronizedMap.put(String.format("%09d", itemsNo) + Thread.currentThread().getName(), "1");
        })), "SynchronizedMap");
        report.put(timeTasks(nThreads, () -> IntStream.range(0, maxItemsPerThread).forEach(itemsNo -> {
            currentHashMap.put(String.format("%09d", itemsNo) + Thread.currentThread().getName(), "1");
        })), "ConcurrentHashMap");

        System.out.println("-------------------------------------------");
        System.out.println(nThreads + "个线程，每个线程同时添加" + maxItemsPerThread + "个元素，最终所需时间如下结果如下:");
        report.forEach((key, value) -> {
            System.out.println(value + ": " + key);
        });
        System.out.println("-------------------------------------------");
        System.out.println("冠军是" + report.firstEntry().getValue());
        System.out.println("-------------------------------------------");
    }

    public static long costTimeWhenPutManyItems(Map<String, String> map, int nThreads, int maxItemsPerThread) throws InterruptedException {

        CountDownLatch startGate = new CountDownLatch(1);
        CountDownLatch endGate = new CountDownLatch(nThreads);
        IntStream.range(0, nThreads).forEach(threadNo -> {
            new Thread(() -> {
                try {
                    startGate.await();
                    IntStream.range(0, maxItemsPerThread).forEach(itemsNo -> {
                        map.put(String.format("%06d%09d", threadNo, itemsNo), "1");
                    });
                } catch (InterruptedException ignored) {
                    // ignored
                } finally {
                    endGate.countDown();
                }

            }).start();
        });
        long startMillions = System.currentTimeMillis();
        startGate.countDown();
        endGate.await();
        long endMillions = System.currentTimeMillis();
        return endMillions - startMillions;
    }

    public static long timeTasks(int nThreads, final Runnable task) throws InterruptedException {
        final CountDownLatch startGate = new CountDownLatch(1);
        final CountDownLatch endGate = new CountDownLatch(nThreads);
        IntStream.range(0, nThreads).forEach(threadNo -> {
            new Thread(() -> {
                try {
                    startGate.await();
                    task.run();
                } catch (InterruptedException ignored) {
                    // ignored
                } finally {
                    endGate.countDown();
                }
            }).start();
        });
        long start = System.nanoTime();
        startGate.countDown();
        endGate.await();
        long end = System.nanoTime();
        return end - start;
    }
}
